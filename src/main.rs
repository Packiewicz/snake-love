#![allow(dead_code)]

extern crate sdl2;
mod common;
mod consts;
mod game;
mod snake;

use consts::{SCREEN_SIZE, TICKRATE};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;

pub fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem
        .window("Snek :~ sss", SCREEN_SIZE.0, SCREEN_SIZE.1)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window.into_canvas().build().map_err(|e| e.to_string())?;

    let creator = canvas.texture_creator();
    let ttf_context = sdl2::ttf::init().map_err(|e| e.to_string())?;

    let mut game = game::Game::new(&creator, &ttf_context).map_err(|e| e.to_string())?;

    let mut event_pump = sdl_context.event_pump()?;
    let mut timer = sdl_context.timer().map_err(|e| e.to_string())?;
    let mut previous_tick = timer.ticks();

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::KeyDown {
                    keycode: Some(keycode),
                    ..
                } => game.key_down(keycode),
                _ => {}
            }
        }

        
        if timer.ticks() - previous_tick > TICKRATE {
            canvas.clear();
            canvas.set_draw_color(Color::RGB(255, 255, 255));

            previous_tick = timer.ticks();

            game.update();
            game.draw(&mut canvas)?;
            if game.is_over() {
                break 'running;
            }
            
            canvas.present();
        }
    }

    Ok(())
}

fn as_u8_slice(v: &[u32]) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts(
            v.as_ptr() as *const u8,
            v.len() * std::mem::size_of::<i32>(),
        )
    }
}
