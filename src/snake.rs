use std::collections::VecDeque;

use sdl2::rect::Rect;

use crate::{common::Vec2d, consts::GAME_OBJECT_SIZE, game::Direction};

pub struct Snake {
    pos_x: i32,
    pos_y: i32,
    score: i32,
    tail: VecDeque<Vec2d<i32>>,
    direction: Direction,
}

impl Snake {
    pub fn new(starting_pos: Vec2d<i32>) -> Snake {
        Snake {
            pos_x: starting_pos.0,
            pos_y: starting_pos.1,
            score: 0,
            tail: VecDeque::new(),
            direction: Direction::Right,
        }
    }

    pub fn move_snake(&mut self, direction: Direction) -> Vec2d<i32> {
        let last_pos = Vec2d::<i32>(self.pos_x, self.pos_y);
        let new_pos = match direction {
            Direction::Down => self.move_down(),
            Direction::Up => self.move_up(),
            Direction::Left => self.move_left(),
            Direction::Right => self.move_right(),
        };

        self.tail.push_front(last_pos);
        if self.tail.len() > self.score as usize {
            self.tail.pop_back();
        }

        self.direction = direction;

        new_pos
    }

    pub fn get_rect(&self) -> Rect {
        Rect::new(self.pos_x, self.pos_y, GAME_OBJECT_SIZE.0, GAME_OBJECT_SIZE.1)
    }

    pub fn add_score(&mut self) {
        self.score += 1
    }

    pub fn pos(&self) -> Vec2d<i32> {
        Vec2d::<i32>(self.pos_x, self.pos_y)
    }

    pub fn tail(&self) -> &VecDeque<Vec2d<i32>> {
        &self.tail
    }
    
    pub fn score(&self) -> i32 { self.score }

    fn move_up(&mut self) -> Vec2d<i32> {
        self.pos_y -= GAME_OBJECT_SIZE.0 as i32;
        Vec2d::<i32>(self.pos_x, self.pos_y)
    }

    fn move_down(&mut self) -> Vec2d<i32> {
        self.pos_y += GAME_OBJECT_SIZE.0 as i32;
        Vec2d::<i32>(self.pos_x, self.pos_y)
    }

    fn move_left(&mut self) -> Vec2d<i32> {
        self.pos_x -= GAME_OBJECT_SIZE.1 as i32;
        Vec2d::<i32>(self.pos_x, self.pos_y)
    }

    fn move_right(&mut self) -> Vec2d<i32> {
        self.pos_x += GAME_OBJECT_SIZE.1 as i32;
        Vec2d::<i32>(self.pos_x, self.pos_y)
    }

    pub fn direction(&self) -> Direction {
        self.direction
    }
}
