use std::path::{self, Path};

use crate::{common::Vec2d, consts::*, snake::Snake};
use rand::prelude::*;
use sdl2::{
    keyboard::Keycode,
    pixels::{Color, PixelFormatEnum},
    rect::Rect,
    render::{Canvas, Texture, TextureAccess, TextureCreator, TextureQuery},
    surface::Surface,
    ttf::{Font, Sdl2TtfContext},
    video::{Window, WindowContext},
};

#[derive(Clone, Copy, PartialEq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub struct Game<'a> {
    // textures
    snake_head_texture: Texture<'a>,
    snake_tail_texture: Texture<'a>,
    food_texture: Texture<'a>,
    font: Font<'a, 'static>,
    texture_creator: &'a TextureCreator<WindowContext>,

    // entities (Sort of..)
    snake: Snake,
    food_pos: Vec2d<u32>,
    food_eaten: bool,
    direction: Direction,
    game_over: bool,

    // rest
    rng: ThreadRng,
}

fn get_texture<'a>(
    texture_creator: &'a TextureCreator<WindowContext>,
    path: &str,
) -> Result<Texture<'a>, String> {
    let surface = Surface::load_bmp(path).map_err(|e| e.to_string())?;
    let t = texture_creator
        .create_texture_from_surface(surface)
        .map_err(|e| e.to_string())?;
    Ok(t)
}

impl<'a> Game<'a> {
    pub fn new(
        texture_creator: &'a TextureCreator<WindowContext>,
        ttf_context: &'a Sdl2TtfContext,
    ) -> Result<Self, String> {
        // load font
        let mut font = ttf_context.load_font("assets/font.TTF", 32)?;
        font.set_style(sdl2::ttf::FontStyle::BOLD);

        Ok(Self {
            snake_head_texture: get_texture(&texture_creator, "assets/snek-head.bmp")?,
            snake_tail_texture: get_texture(&texture_creator, "assets/snek-tail.bmp")?,
            food_texture: get_texture(&texture_creator, "assets/food.bmp")?,
            font,
            texture_creator,

            snake: Snake::new(Vec2d::<i32>(game_map_offset_x() as i32, game_map_offset_y() as i32)),
            food_pos: Vec2d::<u32>(0, 0), // cant generate random pos when random is not initialised yet
            food_eaten: true,
            direction: Direction::Right,
            game_over: false,

            rng: thread_rng(),
        })
    }

    fn get_new_food_pos(&mut self) -> Vec2d<u32> {
        let mut new_pos = self.food_pos.clone();
        // lets shrink rand space so we generate only coordinates that can be translated into rectangle that fits on the sreen and is aligned properly
        let range = (
            GAME_MAP_SIZE.0 / GAME_OBJECT_SIZE.0,
            GAME_MAP_SIZE.1 / GAME_OBJECT_SIZE.1,
        );
        let offset = game_map_offset_y() / GAME_OBJECT_SIZE.1;
        
        while new_pos.0 == self.food_pos.0 && new_pos.1 == self.food_pos.1 {
            if new_pos.0 == self.food_pos.0 {
                new_pos.0 = self.rng.gen_range(0..range.0) * GAME_OBJECT_SIZE.0;
            }

            if new_pos.1 == self.food_pos.1 {
                new_pos.1 = self.rng.gen_range(offset..range.1) * GAME_OBJECT_SIZE.1;
            }
        }
        new_pos
    }

    pub fn draw(&self, canvas: &mut Canvas<Window>) -> Result<(), String> {
        // food
        if !self.food_eaten {
            canvas.copy(
                &self.food_texture,
                None,
                Rect::new(
                    self.food_pos.0 as i32,
                    self.food_pos.1 as i32,
                    GAME_OBJECT_SIZE.0,
                    GAME_OBJECT_SIZE.1,
                ),
            )?;
        }

        let rotate_degree: f64 = match &self.snake.direction() {
            Direction::Down => 90.0,
            Direction::Up => -90.0,
            Direction::Left => 180.0,
            Direction::Right => 0.0,
        };

        // snake head
        canvas.copy_ex(
            &self.snake_head_texture,
            None,
            Rect::new(
                self.snake.pos().0 as i32,
                self.snake.pos().1 as i32,
                GAME_OBJECT_SIZE.0,
                GAME_OBJECT_SIZE.1,
            ),
            rotate_degree,
            None,
            false,
            false,
        )?;

        // snake tail
        for tail_piece in self.snake.tail().iter() {
            canvas.copy(
                &self.snake_tail_texture,
                None,
                Rect::new(
                    tail_piece.0 as i32,
                    tail_piece.1 as i32,
                    GAME_OBJECT_SIZE.0,
                    GAME_OBJECT_SIZE.1,
                ),
            )?;
        }

        // score text
        self.draw_score_line(canvas)?;

        Ok(())
    }

    fn draw_score_line(&self, canvas: &mut Canvas<Window>) -> Result<(), String> {
        let score_text = format!("Score: {}", self.snake.score());
        
        let surface = self.font
            .render(score_text.as_str())
            .blended(Color::RGBA(0, 0, 0, 255))
            .map_err(|e| e.to_string())?;
        let texture = self.texture_creator
            .create_texture_from_surface(&surface)
            .map_err(|e| e.to_string())?;
        let TextureQuery { width, height, .. } = texture.query();
        canvas.copy(
            &texture, 
            None, 
            Rect::new(0, 0, width, height)
        )?;
        Ok(())
    }

    pub fn key_down(&mut self, keycode: Keycode) {
        // in snake you cant go backwards
        let forbidden_direction = match self.direction {
            Direction::Down => Direction::Up,
            Direction::Up => Direction::Down,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
        };

        let prev_direction = self.direction;

        match keycode {
            Keycode::Up => self.direction = Direction::Up,
            Keycode::Down => self.direction = Direction::Down,
            Keycode::Left => self.direction = Direction::Left,
            Keycode::Right => self.direction = Direction::Right,
            _ => return,
        }

        if self.direction == forbidden_direction {
            self.direction = prev_direction;
        }
    }

    pub fn is_over(&self) -> bool {
        self.game_over
    }

    pub fn update(&mut self) {
        if self.food_eaten {
            self.food_eaten = false;
            self.food_pos = self.get_new_food_pos();
        }

        // move snek
        let snake_pos = self.snake.move_snake(self.direction);

        // check if you run into wall or self
        if !self.validate_pos(snake_pos) {
            self.game_over = true;
        }

        // check if food was eaten
        let food_rect = Rect::new(
            self.food_pos.0 as i32,
            self.food_pos.1 as i32,
            GAME_OBJECT_SIZE.0,
            GAME_OBJECT_SIZE.1,
        );
        let snake_rect = self.snake.get_rect();
        if food_rect.contains_rect(snake_rect) {
            self.snake.add_score();
            self.food_eaten = true;
        }
    }

    fn validate_pos(&self, pos: Vec2d<i32>) -> bool {
        if pos.0 < 0 || pos.0 >= SCREEN_SIZE.0 as i32 {
            return false;
        }

        if pos.1 < game_map_offset_y() as i32 || pos.1 >= SCREEN_SIZE.1 as i32 {
            return false;
        }

        for tail_piece in self.snake.tail().iter() {
            if pos.0 == tail_piece.0 && pos.1 == tail_piece.1 {
                return false;
            }
        }

        return true;
    }
}
