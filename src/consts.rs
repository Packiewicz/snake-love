use crate::common::Vec2d;

pub const GAME_MAP_SIZE: Vec2d<u32> = Vec2d::<u32>(640, 400);
pub const GAME_LABEL_SIZE: Vec2d<u32> = Vec2d::<u32>(640, 100);
pub const GAME_OBJECT_SIZE: Vec2d<u32> = Vec2d::<u32>(40, 40); // every element in snake world has same size
pub const SCREEN_SIZE: Vec2d<u32> = Vec2d::<u32>(GAME_MAP_SIZE.0, GAME_MAP_SIZE.1 + GAME_OBJECT_SIZE.1);
pub const TICKRATE: u32 = 250; // ms

pub const fn game_map_offset_x() -> u32 {
    SCREEN_SIZE.0 - GAME_MAP_SIZE.0
}

pub const fn game_map_offset_y() -> u32 {
    SCREEN_SIZE.1 - GAME_MAP_SIZE.1
} 